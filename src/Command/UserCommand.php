<?php

namespace App\Command;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UserCommand extends Command
{
    /** @var EntityManagerInterface */
    protected $entityManager;

    /**
     * UserCommand constructor.
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('demliz:books:user')
            ->setDescription('Create/edit user')
            ->setHelp('Create new user or change password, if user already exists')
            ->addArgument('login', InputArgument::REQUIRED, 'login')
            ->addArgument('password', InputArgument::REQUIRED, 'password')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $user = $this
            ->entityManager
            ->getRepository(User::class)
            ->findOneBy(['login' => $input->getArgument('login')])
        ;
        if (!$user) {
            $user = new User();
            $this->entityManager->persist($user);
            $user->setLogin($input->getArgument('login'));
        }
        $user->setPass(hash('sha256', $input->getArgument('password')));
        $this->entityManager->flush();
        $output->writeln('done');
    }
}
