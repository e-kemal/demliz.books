<?php

namespace App\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="collections")
 */
class Collection
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $user;

    /**
     * @var Book[]|ArrayCollection
     * @ORM\ManyToMany(targetEntity="Book")
     */
    protected $books;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Collection
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Collection
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Book[]
     */
    public function getBooks(): array
    {
        return $this->books;
    }

    /**
     * @param Book[] $books
     * @return Collection
     */
    public function setBooks(array $books): self
    {
        $this->books = $books;
        return $this;
    }

    /**
     * @param Book $book
     * @return Collection
     */
    public function addBook(Book $book): self
    {
        if ($this->books->contains($book)) {
            return $this;
        }
        $this->books[] = $book;
        return $this;
    }

    /**
     * @param Book $book
     * @return Collection
     */
    public function removeBook(Book $book): self
    {
        $this->books->removeElement($book);
        return $this;
    }
}
